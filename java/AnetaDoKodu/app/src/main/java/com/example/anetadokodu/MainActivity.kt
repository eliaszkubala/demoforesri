package com.example.anetadokodu

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        click_me.setOnClickListener(View.OnClickListener {
            Toast.makeText(this, "ha ha ha", Toast.LENGTH_LONG).show()
        })

    }
}
