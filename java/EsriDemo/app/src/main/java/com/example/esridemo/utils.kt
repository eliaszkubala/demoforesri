package com.example.esridemo

import com.esri.arcgisruntime.ArcGISRuntimeEnvironment
import com.esri.arcgisruntime.data.ServiceFeatureTable
import com.esri.arcgisruntime.layers.ArcGISMapImageLayer
import com.esri.arcgisruntime.layers.ArcGISTiledLayer
import com.esri.arcgisruntime.layers.FeatureLayer
import com.esri.arcgisruntime.layers.Layer
import com.esri.arcgisruntime.security.UserCredential

class utils {
    companion object {

        fun license() {
            ArcGISRuntimeEnvironment.setLicense("runtimelite,1000,rud5462687587,none,XXMFA0PL4P4M8YAJM246")
            ArcGISRuntimeEnvironment.initialize()
        }

        fun getCredentials(): UserCredential {
            return UserCredential(
                "guest",
                "guest",
                "https://sip2.police.pl/gplogin_service/gprest/service/generateToken"
            )
        }

        fun getLayer(): Layer {
            val layer =
                ArcGISMapImageLayer("https://sip2.police.pl/gprest/services/IMAP/OSM_v2/MapServer")
            layer.credential = getCredentials()
            return layer
        }

        fun getLayerBase(): Layer {
            val url = "https://osm.imapcity.pl/maps/services/OSM_GP_V2_C_92/MapServer"
            //val url = "http://mapy.geoportal.gov.pl/gprest/services/G2_MOBILE_500/MapServer"
            return ArcGISTiledLayer(url)
        }

        fun getFeatureLayer(): Layer {
            val url =
                "https://sip.um.swidnica.pl/ims/rest/services/s_ZGL_zgloszenia_publ/FeatureServer/2"
            val serviceFeatureTable = ServiceFeatureTable(url)
            serviceFeatureTable.displayName = url
            return FeatureLayer(serviceFeatureTable)
        }
    }
}