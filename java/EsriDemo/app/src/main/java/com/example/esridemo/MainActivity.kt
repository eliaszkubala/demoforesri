package com.example.esridemo

import android.Manifest
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.esri.arcgisruntime.geometry.SpatialReference
import com.esri.arcgisruntime.layers.Layer
import com.esri.arcgisruntime.loadable.LoadStatus
import com.esri.arcgisruntime.mapping.ArcGISMap
import com.esri.arcgisruntime.mapping.view.LocationDisplay
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    private val mSr = SpatialReference.create(2180)
    private val TAG = javaClass.simpleName

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if (mapView != null) {
            val map =
                //  ArcGISMap(Basemap.Type.STREETS_VECTOR, 34.0270, -118.8050, 13)
                ArcGISMap(mSr)
            mapView.map = map
            utils.license()
            mapView.map.operationalLayers.add(lis(utils.getLayerBase()))
        }

        center()

        val featureLayer = utils.getFeatureLayer()

        mapView.map.operationalLayers.add(lis(featureLayer))
        featureLayer.isVisible = true
        featureLayer.opacity = .5f



        center.setOnClickListener { center() }
    }

    override fun onResume() {
        super.onResume()
        if (!locationGranted()) {
            ActivityCompat.requestPermissions(
                this, arrayOf(
                    Manifest.permission.ACCESS_COARSE_LOCATION,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ), 0
            )
        } else {
            center()
        }

        mapView.let { it.resume() }
    }

    override fun onDestroy() {
        super.onDestroy()
        mapView.let { it.dispose() }
    }

    override fun onPause() {
        super.onPause()
        mapView.let { it.pause() }
    }

    private fun center() {
        if (locationGranted()) {
            Log.d(TAG, "center to position")

            if (!mapView.locationDisplay.isStarted) {
                Log.d(TAG, "centruje")
                mapView.locationDisplay.autoPanMode = LocationDisplay.AutoPanMode.RECENTER;
                mapView.locationDisplay.startAsync()
            }

            mapView.locationDisplay.addLocationChangedListener {
                //Log.d(TAG, "locationDisplay: " + it.location.position.toJson())
            }

            mapView.locationDisplay.location?.position?.let {
                Log.d(TAG, "position: " + it.toJson())
            }

            Log.d(TAG, "locationDisplayIsStarted: " + mapView.locationDisplay.isStarted)
        }
    }

    fun lis(layer: Layer): Layer {
        layer.addLoadStatusChangedListener {
            if (it.newLoadStatus == LoadStatus.LOADING) {
                Log.d(TAG, "Loading... " + layer.name)
            }

            if (it.newLoadStatus == LoadStatus.LOADED) {
                Log.d(TAG, "Loaded " + layer.name)
            }

            if (it.newLoadStatus == LoadStatus.FAILED_TO_LOAD) {
                Log.d(TAG, "Cannot load feature layer: " + layer.name)

                layer.loadError?.additionalMessage?.let { Log.d(TAG, "additional msg: " + it) }
                layer.loadError?.message?.let { Log.d(TAG, "msg" + it) }
            }
        }
        return layer
    }

    private fun locationGranted(): Boolean {
        return ContextCompat.checkSelfPermission(
            this,
            Manifest.permission.ACCESS_COARSE_LOCATION
        ) == PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(
                    this,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) == PackageManager.PERMISSION_GRANTED
    }


}